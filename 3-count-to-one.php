<?php

function countToOne($number)
{
    if ($number < 1 || !is_numeric($number)) {
        return 'Masukan angka yang valid';
    }

    $counting = [];
    for ($i=1; $i <= $number; $i++) { 
        $ii = $i;
        $countNow = 0;
        while ($ii > 1) {
            if ($ii%2 == 0) {
                $ii = $ii/2;
            } else {
                $ii = (3*$ii) + 1;
            }
            $countNow += 1;
        }
        $counting[$i] = $countNow;
    }
    
    return 'Angka dengan operasi terbanyak adalah : ' . implode(', ', array_keys($counting, max($counting)));
}

$number = readline("Masukan angka: ");
echo countToOne($number);
