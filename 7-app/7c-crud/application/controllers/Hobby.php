<?php
defined('BASEPATH') OR die('No direct script access allowed');

class Hobby extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Hobby_Model', 'hobby');
    }

    public function index()
    {
        $data['all'] = $this->hobby->getAll();
        $data['categories'] = $this->hobby->getCategories();
        $data['hobbies'] = $this->hobby->getHobbies();
        $this->load->view('data', $data);
    }

    public function get_hobbies()
    {
        $idCat = $this->uri->segment(3);
        $data = $this->hobby->getHobbies($idCat);
        $options = '';
        foreach ($data as $v) {
            $options .= "<option value=\"$v->id\">$v->name</option>";
        }

        echo $options;
    }

    public function store()
    {
        header('Content-Type: application/json');
        $post = $this->input->post();
        $data = [
            'name' => $post['name'],
            'id_hobby' => $post['hobby'],
            'id_category' => $post['category']
        ];
        $result = (array) $this->hobby->insertData($data);
        if ($result) {
            $return = [
                'title' => 'success',
                'data' => $result,
                'msg' => 'Data berhasil ditambahkan!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }

    public function update()
    {
        header('Content-Type: application/json');
        $post = $this->input->post();
        $id = $post['id'];
        $data = [
            'name' => $post['name'],
            'id_hobby' => $post['hobby'],
            'id_category' => $post['category']
        ];
        $result = (array) $this->hobby->updateData($data, $id);
        if ($result) {
            $return = [
                'title' => 'success',
                'data' => $result,
                'msg' => 'Data ' . $post['name'] . ' berhasil diubah!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }

    public function destroy()
    {
        header('Content-Type: application/json');
        $id = $this->uri->segment(3);
        $data = $this->hobby->getSpecific($id);
        $result = $this->hobby->deleteData($id);
        if ($result) {
            $return = [
                'title' => 'success',
                'msg' => 'Data ' . $data->user_name . ' berhasil dihapus!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }
}

