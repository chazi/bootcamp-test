<?php
defined('BASEPATH') OR die('No direct script access allowed!');

class Hobby_Model extends CI_Model
{
    public function getAll()
    {
        $this->db->select('u.*, u.id AS id_user, u.name AS user_name, h.name AS hobby_name, c.name AS category_name');
        $this->db->join('hobi h', 'u.id_hobby = h.id');
        $this->db->join('kategori c', 'h.id_category = c.id');
        $data = $this->db->get('nama u');
        return $data->result();
    }

    public function getSpecific($id)
    {
        $this->db->select('u.*, u.name AS user_name, h.name AS hobby_name, c.name AS category_name, u.id AS user_id');
        $this->db->join('hobi h', 'u.id_hobby = h.id');
        $this->db->join('kategori c', 'h.id_category = c.id');
        $this->db->where('u.id', $id);
        $data = $this->db->get('nama u');
        return $data->row();
    }

    public function getHobbies($id_cat = 0)
    {
        if ($id_cat !== 0) {
            $this->db->where('id_category', $id_cat);
        }
        $data = $this->db->get('hobi');
        return $data->result();
    }

    public function getCategories()
    {
        $data = $this->db->get('kategori');
        return $data->result();
    }

    public function insertData($data)
    {
        $this->db->insert('nama', $data);
        return $this->getSpecific($this->db->insert_id());
    }

    public function updateData($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('nama', $data);
        return $this->getSpecific($id);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('nama');
    }
}

