<?php

/**
 * Untuk menjalankan file, 
 * Silahkan import dahulu file sql yang ada di ./db/hobby.sql kedalam database
 * File ini dijalankan dengan PHP 7.1 menggunakan Ampps
 * Database yang digunakan MySQL MariaDB
 */

// silahkan ganti password sesuai konfigurasi database server
$koneksi = mysqli_connect('localhost', 'root', 'mysql', 'hobby');

$query = mysqli_query($koneksi, "SELECT u.name AS user_name, h.name AS hobby_name, c.name AS category_name FROM nama u JOIN hobi h ON u.id_hobby = h.id JOIN kategori c ON h.id_category = c.id");
?>

<table border="1">
    <thead>
        <th>name</th>
        <th>hobby</th>
        <th>category</th>
    </thead>
    <tbody>
        <?php while ($row = mysqli_fetch_assoc($query)): ?>
            <tr>
                <td><?= $row['user_name'] ?></td>
                <td><?= $row['hobby_name'] ?></td>
                <td><?= $row['category_name'] ?></td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>
