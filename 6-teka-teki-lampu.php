<?php

function solution($saklar)
{
    if (strlen($saklar) > 15) {
        return 'Masukan tidak boleh lebih dari 15 digit';
    }

    $kondisiLampu = array_fill(1, 15, false);
    $sakArr = str_split($saklar);
    
    for ($i=0; $i < strlen($saklar); $i++) { 
        if (!in_array($sakArr[$i], [1, 2, 3])) {
            return 'Masukan hanya angka 1, 2, dan 3';
        }

        for ($j=1; $j <= count($kondisiLampu); $j++) { 
            if ($j%$sakArr[$i] == 0) {
                $kondisiLampu[$j] = !$kondisiLampu[$j];
            }
        }
    }

    $return = var_export($kondisiLampu) . PHP_EOL;
    $return .= "Jumlah Lampu Menyala = " . count(array_keys($kondisiLampu, true)) . PHP_EOL;
    $return .= "Jumlah Lampu Mati = " . count(array_keys($kondisiLampu, false)) . PHP_EOL;

    return $return;
}

$input = readline("Masukan input: ");
echo solution($input);
