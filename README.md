## Readme


### Kegunaan JSON untuk REST API
Penggunaan JSON dalam REST API adalah sebagai media pertukaran data antara server dan client.
JSON dipilih karena bisa digunakan di hampir semua bahasa pemrograman dan penulisan yang mudah


### Syntax yang saya gunakan adalah :

- PHP

- MySQl
  

### Aplikasi yang diperlukan untuk menjalankan program:

- Web server PHP dan MySQl, seperti AMPPS, XAMPP, WAMM

- CMD / Terminal
  

### Aplikasi Crud
> Aplikasi menggunakan framework Codeigniter 3.10 <br>
> Folder aplikasi berada di `./7-app/7c-crud/` <br>
> Untuk database bisa diimport dari file `./7-app/database/hobby.sql` <br>
> Ubah konfigurasi database di `./7-app/7c-crud/application/config/database.php` <br>
  

### ScreenShoot

- Index ![index](7-app/assets/img/screenshoot/index.png)

- Add ![add](7-app/assets/img/screenshoot/add.png)

- Edit ![edit](7-app/assets/img/screenshoot/edit.png)

- Delete ![delete](7-app/assets/img/screenshoot/delete.png)

  