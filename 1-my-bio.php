<?php

function my_bio()
{
    $my_bio = [
        'name' => 'Muhamad Chairul Aziz',
        'age' => 17,
        'address' => 'Jl. Pertiwi. No 54E, Susukan, Ciracas, Jakarta Timur',
        'hoobies' => [
            'coding',
            'film',
            'anime'
        ],
        'is_married' => false,
        'list_school' => [
            [
                'name' => 'SDN 05 Pagi',
                'year_in' => 2007,
                'year_out' => 2013,
                'major' => null
            ],
            [
                'name' => 'SMP Negeri 174 SSN Jakarta',
                'year_in' => 2013,
                'year_out' => 2016,
                'major' => null
            ],
            [
                'name' => 'SMK Negeri  22 Jakarta',
                'year_in' => 2016,
                'year_out' => 2019,
                'major' => null
            ]
        ],
        'sklills' => [
            ['skill_name' => 'PHP', 'level' => 'advanced'],
            ['skill_name' => 'JS', 'level' => 'beginner'],
            ['skill_name' => 'CSS', 'level' => 'advanced'],
            ['skill_name' => 'Linux', 'level' => 'beginner'],
        ],
        'interest_in_coding' => true
    ];

    header('Content-Type: application/json');
    return json_encode($my_bio);
}

echo my_bio();
