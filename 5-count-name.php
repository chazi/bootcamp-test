<?php

function countName($name)
{
    if (!preg_match("/[A-Za-z]i/", $name)) {
        return 'Masukan hanya huruf';
    }

    $alpha = array_combine(range('a', 'z'), range(1, 26));
    $word = explode(' ', strtolower($name));
    $nameArr = [];

    for ($i=0; $i < count($word); $i++) { 
        $let = str_split($word[$i]);
        $sum = 0;
        foreach ($let as $l) {
            $sum += $alpha[$l];
        }

        $nameArr[] = $sum*($i+1);
    }

    return "[" . implode(', ', $nameArr) . "]";
}

$name = readline("Your Name: ");
echo countName($name);
