<?php

function is_username_valid($username)
{
    if (preg_match("/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[a-zA-Z0-9]){5,9}/", $username)) {
        return true;
    }

    return false;
}

function is_password_valid($password)
{
    if (preg_match("/^(?=.*[\D\d])(?=.@)(?=.*[\D\d]){9,}/", $password)) {
        return true;
    }

    return false;
}

var_dump(is_username_valid('Ayu99z'));
var_dump(is_password_valid('C0d3YourFuture!#'));
